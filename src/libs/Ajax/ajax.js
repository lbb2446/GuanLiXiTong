import axios from 'axios'

// const ERRORMSG = {
//   8200: '请求操作成功',
//   8401: '不允许的操作',
//   8402: '请求参数非法',
//   8403: '权限校验错误',
//   8404: '没有找到资源',
//   8405: '请求数据不存在',
//   8406: '请求已超时',
//   8407: 'token已经过期',
//   8408: '无效的token',
//   8410: '重复操作',
//   8500: '服务器内部错误'
// }
let base={
	url:"http://101.132.156.26:1000"
}
const instance = axios.create({
  baseURL: "http://101.132.156.26:1000",
  timeout: 30000,
  headers: {
    'Content-Type': 'application/json;charset:utf-8'
  },
  params: {
  },
  validateStatus: function (status) {
    return status >= 200 && status < 500 // default
  }

})

const apiMethods = ['get', 'post', 'put', 'delete']
const _default = {

  $performance: false,
  $logwarn: false,
  $log: false,
  $msg: {
    error: undefined,
    loading: undefined,
    before: undefined,
    success: undefined,
    nodata: undefined
  },

  $loadingbar: false,
  $loadingbtn: false
}

function Ajax (opt) {
  
  const method =opt.method||"post"
  const option = { ..._default,
    ...opt
  }
  
  if (opt.url.indexOf('http') === -1) {
    option.url = base.url + opt.url
  }

  option.contentType="application/json; charset=utf-8";
  
  if(method!=undefined){
  	  option.type=opt.method
  }
  if(method!="get"&&method!="GET"){
  	option.data=JSON.stringify(option.data)
  }

   return $.ajax(option);
  
//var mock=require('./../API/index.js').default
//console.log(opt.url)
//if (opt.url.indexOf('http') === -1) {
//  var _url=  opt.url;
//  if(opt.url.indexOf("/")!==-1){
//    _url=opt.url.replace("/","")
//
//  }
//
//  if(mock[_url]!=undefined){
//
//    console.log("执行了mock",mock[_url])
//   return  new Promise(function(resolve, reject) {
//     resolve(mock[_url]);
//    });
//  }else{
//
//  }
//}else{
//	 var _url=  opt.url;
//if(mock[_url]!=undefined){
//
//    console.log("执行了mock",mock[_url])
//   return  new Promise(function(resolve, reject) {
//     resolve(mock[_url]);
//    });
//  }else{
//
//  }
//}

 if(!option.$simple){
 	
 


  if (opt.url.indexOf('http') === -1) {
    opt.url = base.url + opt.url
  }

  if (option.$msg.loading !== undefined) {
    this.$Message.loading({
      content: option.$msg.loading,
      duration: 0
    })
  }
  if (option.$msg.before !== undefined) {
    this.$Message.info(option.$msg.before)
  }
 }
  const applyArgu = (method === 'get' || method === 'delete') ? [opt.url, {
    params: {
      token: window.sessionStorage.getItem('token')
    }
  }] : [opt.url, opt.data, {
    params: {
      token: window.sessionStorage.getItem('token')
    }
  }]

  let oldHash = window.location.hash
  return instance[method].apply(null, applyArgu).then((response) => {
  	 return Promise.resolve(response.data)
    if(oldHash != window.location.hash) {
			return;
		}
    if (option.$performance) {
      console.timeEnd(option.url + '请求时间')
    }

    const {
				data: _data
			} = response
    const {
				isSuccess,
				errCode,
				message,
				data
			} = _data
    if (isSuccess === true) {
      if (option.$msg.nodata !== undefined) {
        if (typeof data.data === 'Array' && data.data.length == 0) {
          this.$Notice.warning({
            title: '信息',
            desc: option.$msg.nodata
          })
        }
      }

      if (option.$msg.success !== undefined) {
        this.$Notice.success({
          title: '信息',
          desc: option.$msg.success
        })
      }

      if (option.$performance) {
        console.timeEnd(option.url + '总共请求-回调完成时间')
      }
      return Promise.resolve(_data)
    } else {
      if (option.$msg.error !== undefined) {
        this.$Notice.error({
          title: '错误信息',
          desc: option.$msg.error
        })
      }

      if (errCode === 9527) {
        window.sessionStorage.removeItem('token')
        this.$router.replace({
          path: '/login'
        })
      }
      return Promise.reject({
        data: data,
        errCode: errCode
      })
    }
  })
		.catch((error) => {
  if (!option.$msg) {
    console.error(option.$msg + ',服务器挂了!')
  }
  return Promise.reject(error)
})
}

export default Ajax
